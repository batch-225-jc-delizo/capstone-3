import React, { useState, useContext } from 'react';
import { Card, Button, Image, Modal } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';
import { useNavigate } from 'react-router-dom';
import '../App.css';

export default function BookCard({ bookProp }) {
  const { name, description, price, author, genre, image, _id } = bookProp;
  // const [isAddedToCart, setIsAddedToCart] = useState(false);
  // // const [showModal, setShowModal] = useState(false);
  // const { user } = useContext(UserContext);
  const navigate = useNavigate();
  // const accessToken = localStorage.getItem('access');

  // const handleAddToCart = async () => {
  //   setIsAddedToCart(true);
  //   try {
  //     if (!accessToken) {
  //       // User is not logged in, navigate to login page
  //       navigate('/login');
  //       return;
  //     }

  //     const response = await fetch(`${process.env.REACT_APP_API_URL}/booknook/cart/add-to-cart/${bookProp._id}`, {
  //       method: 'POST',
  //       headers: {
  //         'Content-Type': 'application/json',
  //         Authorization: `Bearer ${accessToken}`
  //       },
  //       body: JSON.stringify({ bookId: _id, quantity: 1 }),
  //     });
  //     if (!response.ok) {
  //       throw new Error('Failed to add item to cart');
  //     }
  //   } catch (error) {
  //     console.error(error);
  //   } finally {
  //     setTimeout(() => setIsAddedToCart(false), 2000);
  //   }
  // };

  // const handleCloseModal = () => setShowModal(false);

  return (
      <Card onClick={() => navigate(`/bookpage/${_id}`)} className='hoverme bookstore-card'
        style={{
          maxWidth: '200px',
          marginLeft: '2px',
          marginRight: '2px',
          marginTop: '10px',
          boxShadow: '0px 4px 8px rgba(0, 0, 0, 0.15)',
          transition: 'transform 0.15s ease-in-out',
        }}

      >
        <Image src={image} alt={image} fluid style={{ height: '150px', objectFit: 'cover', cursor: 'pointer' }} className='bookstore-img' />
        <Card.Header
          style={{
            fontWeight: 'bold',
            WebkitLineClamp: '1',
            overflow: 'hidden',
            textOverflow: 'ellipsis',
            display: '-webkit-box',
            WebkitBoxOrient: 'vertical',
            fontFamily: "Gentium Book Plus",
            paddingLeft: '10px',
            paddingRight: '10px',
            paddingTop: '5px',
            paddingBottom: '5px',
            cursor: 'pointer',
          }}
        >
          {name}
        </Card.Header>
        <Card.Body className='bookstore-desc'
          style={{
            paddingLeft: '10px',
            paddingRight: '10px',
            paddingTop: '5px',
            paddingBottom: '5px',
            cursor: 'pointer',
          }}
        >
          <Card.Text
            style={{
              WebkitLineClamp: '2',
              overflow: 'hidden',
              textOverflow: 'ellipsis',
              display: '-webkit-box',
              WebkitBoxOrient: 'vertical',
              fontSize: '12px',
              padding: 0,
              margin: 0
            }}
          >
            {description}
          </Card.Text>
        </Card.Body>
        <Card.Footer style={{ 
          display: 'flex', 
          justifyContent: 'space-between', 
          cursor: 'pointer',
          paddingLeft: '10px',
          paddingRight: '10px',
          paddingTop: '5px',
          paddingBottom: '5px', 
        }}>
          <Card.Text
            style={{
              margin: 0,
              padding: 0,
              fontWeight: 'bold',
              fontFamily: 'Roboto',
              color: '#F78124'
            }}
          >
            ₱{price}.00
          </Card.Text>
          {/* {!user.isAdmin && (
            <Button variant={isAddedToCart ? 'success' : 'outline-primary'} onClick={handleAddToCart}>
              {isAddedToCart ? <>&#x2714;</> : <>&#128722;</>}
            </Button>
          )} */}

        </Card.Footer>
      </Card>
  );

}  