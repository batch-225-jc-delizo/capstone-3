import React from 'react'
import { Button, Modal } from 'react-bootstrap';
import { FaTrash, FaEye, FaEyeSlash, FaQuestion, FaSortAlphaDown, FaSortAlphaUp } from 'react-icons/fa';

const AdminInfo = ({ show, onClose }) => {
    return (
        <Modal show={show} onHide={onClose}>
            <Modal.Header closeButton>
                <Modal.Title style={{fontFamily: "Gentium Book Plus", fontWeight: 'bold'}}>Page Information</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <p>To <b>search</b> for a specific book, type the name of the book in the search bar located at the top of the page. You can also sort the list of books by name in ascending or descending order by clicking on the <FaSortAlphaUp /> button. <br /><br /> 
                To <b>update</b> a book, click on the image or name area of the book card you wish to update. This will open the update modal where you can make changes to the book's information. <br /><br /> To <b>delete</b> a book from the archive, click on the "delete" button located on the card of the book you wish to remove. A confirmation prompt will appear asking you to confirm your action. <br /><br /> To <b>disable or enable</b> a book, click on the "Eye" button located on the bottom of the book you wish to change. Disabled Books will not show in the book store. <br /><br /> To <b>add</b> a new book to the archive, click on the "add new book" button located at the top right corner of the page. This will open the add new book modal where you can enter the details of the new book.</p>
            </Modal.Body>
        </Modal>

    )
}

export default AdminInfo;
