import React, { useState } from 'react';
import { Modal, Button, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function UpdateABook(props) {
  const [name, setName] = useState(props.book.name);
  const [description, setDescription] = useState(props.book.description);
  const [author, setAuthor] = useState(props.book.author);
  const [genre, setGenre] = useState(props.book.genre);
  const [price, setPrice] = useState(props.book.price);
  const [image, setImage] = useState(props.book.image); // add image state variable
  const accessToken = localStorage.getItem('access')

  const handleNameChange = (event) => {
    setName(event.target.value);
  };

  const handleDescriptionChange = (event) => {
    setDescription(event.target.value);
  };

  const handleAuthorChange = (event) => {
    setAuthor(event.target.value);
  };

  const handleGenreChange = (event) => {
    setGenre(event.target.value);
  };

  const handlePriceChange = (event) => {
    setPrice(event.target.value);
  };

  const handleImageChange = (event) => { // add function to handle image change
    setImage(event.target.value);
  }

  const handleSubmit = (event) => {
    event.preventDefault();

    const updatedBook = {
      ...props.book,
      name,
      description,
      author,
      genre,
      price,
      image // add image field to updated book object
    };

    fetch(`${process.env.REACT_APP_API_URL}/booknook/books/${props.book._id}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${accessToken}`,
      },
      body: JSON.stringify(updatedBook),
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
        return response.json();
      })
      .then((data) => {
        console.log('Book updated:', data);
        props.onClose();
        props.onBookUpdated(updatedBook);
        Swal.fire({
          icon: 'success',
          title: 'Book updated',
          showConfirmButton: false,
          timer: 1500,
        });
      })
      .catch((error) => {
        console.error('Error updating book:', error);
        Swal.fire({
          icon: 'error',
          title: 'Error updating book',
          text: 'Please try again later.',
        });
      });
  };

  return (
    <Modal show={props.show} onHide={props.onClose}>
      <Modal.Header closeButton>
        <Modal.Title style={{ fontFamily: "Gentium Book Plus", fontWeight: "bold" }}>Updating "{props.book.name}"</Modal.Title>
      </Modal.Header>
      <Form onSubmit={handleSubmit}>
        <Modal.Body>
          <Form.Group>
            <Form.Label style={{ fontFamily: "Gentium Book Plus", fontWeight: "bold" }}>Name:</Form.Label>
            <Form.Control type="text" placeholder={props.book.name} value={name} onChange={handleNameChange} style={{ fontFamily: "Gentium Book Plus" }} />
          </Form.Group>
          <Form.Group>
            <Form.Label style={{ fontFamily: "Gentium Book Plus", fontWeight: "bold" }}>Description:</Form.Label>
            <Form.Control type="text" placeholder={props.book.description} as="textarea" rows={5} value={description} onChange={handleDescriptionChange} style={{ fontFamily: "Gentium Book Plus" }} />
          </Form.Group>
          <Form.Group>
            <Form.Label style={{ fontFamily: "Gentium Book Plus", fontWeight: "bold" }}>Author:</Form.Label>
            <Form.Control type="text" placeholder={props.book.author} value={author} onChange={handleAuthorChange} style={{ fontFamily: "Gentium Book Plus" }} />
          </Form.Group>
          <Form.Group>
            <Form.Label style={{ fontFamily: "Gentium Book Plus", fontWeight: "bold" }}>Genre:</Form.Label>
            <Form.Control type="text" placeholder={props.book.genre} value={genre} onChange={handleGenreChange} style={{ fontFamily: "Gentium Book Plus" }} />
          </Form.Group>
          <Form.Group>
            <Form.Label style={{ fontFamily: "Gentium Book Plus", fontWeight: "bold" }}>Price:</Form.Label>
            <Form.Control type="number" placeholder={props.book.price} value={price} onChange={handlePriceChange} />
          </Form.Group>
          <Form.Group>
            <Form.Label style={{ fontFamily: "Gentium Book Plus", fontWeight: "bold" }}>Image:</Form.Label>
            <Form.Control type="text" placeholder={props.book.image} value={image} onChange={handleImageChange} style={{ fontFamily: "Gentium Book Plus" }} />
          </Form.Group>
        </Modal.Body>
        <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
          <Button 
            variant="secondary" 
            onClick={props.onClose} 
            style={{ 
              borderRadius: '0 0 0 5px',
              flex: 1, 
              fontWeight: 'bold', 
              fontFamily: 'Gentium Book Plus', 
              fontSize: '20px'
            }}>Close
          </Button>
          <Button 
            variant="success" 
            type="submit"
            style={{ 
              borderRadius: '0 0 5px 0',
              flex: 1, 
              fontWeight: 'bold', 
              fontFamily: 'Gentium Book Plus', 
              fontSize: '20px'
            }}>Save Changes
          </Button>
        </div>
      </Form>
    </Modal>
  );
}  