import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { Link, NavLink } from 'react-router-dom';
import { useContext, useEffect, useState } from 'react';
import UserContext from '../UserContext';
import handleLogout from '../pages/Logout';
import './AppNavbar.css';
import readwell from '../assets/readwell.svg';
import { FaUserCircle, FaSignInAlt, FaUserPlus, FaSignOutAlt } from 'react-icons/fa';
import { Dropdown } from 'react-bootstrap';
import '../App.css';

export default function AppNavbar() {
	const { user } = useContext(UserContext);
	const accessToken = localStorage.getItem('access');

	const userLinks = (
		<>
			<Nav.Link as={NavLink} to="/cart">
				<span className='navright px-3 px-md-0' style={{ fontFamily: 'Gentium Book Plus', fontWeight: 'bold', fontSize: '20px', marginRight: '12px' }}>Cart</span>
			</Nav.Link>
			<Nav.Link as={NavLink} to="/orders">
				<span className='navright px-3 px-md-0' style={{ fontFamily: 'Gentium Book Plus', fontWeight: 'bold', fontSize: '20px', marginRight: '12px' }}>Orders</span>
			</Nav.Link>
			<Nav.Link as={NavLink} to="/logout" onClick={handleLogout} style={{ fontFamily: 'Gentium Book Plus', fontWeight: 'bold', fontSize: '20px', marginRight: '12px' }}>
				<span className='navright px-3 px-md-0'><FaSignOutAlt /></span>
			</Nav.Link>
		</>
	);

	const adminLinks = (
		<>
			<Nav.Link as={NavLink} to="/book-archives">
				<span className='navright px-3 px-md-0' style={{ fontFamily: 'Gentium Book Plus', fontWeight: 'bold', fontSize: '20px', marginRight: '12px' }}>Book Archives</span>
			</Nav.Link>
			<Nav.Link as={NavLink} to="/view-orders">
				<span className='navright px-3 px-md-0' style={{ fontFamily: 'Gentium Book Plus', fontWeight: 'bold', fontSize: '20px', marginRight: '12px' }}>Orders</span>
			</Nav.Link>
			<Nav.Link as={NavLink} to="/view-users">
				<span className='navright px-3 px-md-0' style={{ fontFamily: 'Gentium Book Plus', fontWeight: 'bold', fontSize: '20px', marginRight: '12px' }}>Users</span>
			</Nav.Link>
			<Nav.Link as={NavLink} to="/logout" onClick={handleLogout}>
				<span className='navright px-3 px-md-0' style={{ fontFamily: 'Gentium Book Plus', fontWeight: 'bold', fontSize: '20px', marginRight: '12px' }}><FaSignOutAlt /></span>
			</Nav.Link>
		</>
	);

	const guestLinks = (
		<>
			<Dropdown drop="start" className='navright px-3 px-md-0' style={{ marginRight: '10px', marginLeft: '10px'}}>
				<Dropdown.Toggle as={NavLink} id="dropdown-basic" style={{ color: '#343541' }} className='navright px-3 px-md-0'>
					<span style={{ color: '#343541', fontSize: '23px'}}>
						<FaUserCircle />
					</span>
				</Dropdown.Toggle>

				<Dropdown.Menu style={{ paddingBottom: '0', paddingTop: '0' }}>
					<style>
						{` .dropdown-item.active .fa-icon, .dropdown-item:active .fa-icon { color: #fff; /* change this to the desired color */ } .dropdown-item .fa-icon { color: #343541; /* default color */ } .dropdown-item.active, .dropdown-item:active { background-color: #444654; /* change this to the desired color */ } `}
					</style>
					<Dropdown.Item as={NavLink} to="/login" style={{ paddingBottom: '10px' }}>
						<span style={{ color: '#343541', fontSize: '23px', marginRight: '10px' }}>
							<FaSignInAlt className="fa-icon" />
						</span>
						<span style={{ fontSize: '18px', fontFamily: 'Gentium Book Plus', fontWeight: 'bold' }}>Login</span>
					</Dropdown.Item>
					<Dropdown.Item as={NavLink} to="/register" variant="secondary" style={{ paddingBottom: '10px' }}>
						<span style={{ color: '#343541', fontSize: '23px', marginRight: '10px' }}>
							<FaUserPlus className="fa-icon" />
						</span>
						<span style={{ fontSize: '18px', fontFamily: 'Gentium Book Plus', fontWeight: 'bold' }}>Register</span>
					</Dropdown.Item>
				</Dropdown.Menu>
			</Dropdown>

		</>
	);

	let links;
	if (user.isAdmin) {
		links = (
			<Nav className="ms-auto">
				<Nav.Link as={NavLink} to="/">
					<span className='navright px-3 px-md-0' style={{ fontFamily: 'Gentium Book Plus', fontWeight: 'bold', fontSize: '20px', marginRight: '12px' }}>Home</span>
				</Nav.Link>
				<Nav.Link as={NavLink} to="/books">
					<span className='navright px-3 px-md-0' style={{ fontFamily: 'Gentium Book Plus', fontWeight: 'bold', fontSize: '20px', marginRight: '12px' }}>Book Store</span>
				</Nav.Link>
				{adminLinks}
			</Nav>
		);
	} else if (user.id || accessToken) {
		links = (
			<Nav className="ms-auto">
				<Nav.Link as={NavLink} to="/">
					<span className='navright px-3 px-md-0' style={{ fontFamily: 'Gentium Book Plus', fontWeight: 'bold', fontSize: '20px', marginRight: '12px' }}>Home</span>
				</Nav.Link>
				<Nav.Link as={NavLink} to="/books">
					<span className='navright px-3 px-md-0' style={{ fontFamily: 'Gentium Book Plus', fontWeight: 'bold', fontSize: '20px', marginRight: '12px' }}>Book Store</span>
				</Nav.Link>
				{userLinks}
			</Nav>
		);
	} else {
		links = (
			<Nav className="ms-auto">
				<Nav.Link as={NavLink} to="/">
					<span className='navright px-3 px-md-0' style={{ fontFamily: 'Gentium Book Plus', fontWeight: 'bold', fontSize: '20px', marginRight: '12px' }}>Home</span>
				</Nav.Link>
				<Nav.Link as={NavLink} to="/books">
					<span className='navright px-3 px-md-0' style={{ fontFamily: 'Gentium Book Plus', fontWeight: 'bold', fontSize: '20px', marginRight: '12px' }}>Book Store</span>
				</Nav.Link>
				{guestLinks}
			</Nav>
		);
	}

	const [prevScrollPos, setPrevScrollPos] = useState(0);
	const [visible, setVisible] = useState(true);

	useEffect(() => {
		const handleScroll = () => {
			const currentScrollPos = window.pageYOffset;
			const visible = prevScrollPos > currentScrollPos || currentScrollPos < 10;
			setVisible(visible);
			setPrevScrollPos(currentScrollPos);
		};


		window.addEventListener('scroll', handleScroll);

		return () => window.removeEventListener('scroll', handleScroll);
	}, [prevScrollPos, visible]);

	return (
		<Navbar
			bg="light"
			expand="lg"
			className={`fixed-top ${visible ? 'navbar-show' : 'navbar-hide'}`}
		>
			<Navbar.Brand as={NavLink} to="/">
				<img src={readwell} alt="Logo" style={{ maxHeight: '30px', marginLeft: '15px' }} />
			</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">{links}</Navbar.Collapse>
		</Navbar>
	);
}
