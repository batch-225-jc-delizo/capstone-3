import React, { useState } from 'react';
import { Modal, Button, Form, ButtonGroup, ToggleButton } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AddBook(props) {
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [author, setAuthor] = useState('');
  const [genre, setGenre] = useState('');
  const [price, setPrice] = useState(0);
  const [image, setImage] = useState(null);
  const [isActive, setIsActive] = useState(true);
  const [isLoading, setIsLoading] = useState(false);
  const accessToken = localStorage.getItem('access')

  const handleSubmit = (event) => {
    event.preventDefault();

    setIsLoading(true);

    const newBook = {
      name: name,
      description: description,
      author: author,
      genre: genre,
      price: price,
      isActive: isActive,
      image: image,
    };

    fetch(`${process.env.REACT_APP_API_URL}/booknook/books/add-book`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${accessToken}`,
      },
      body: JSON.stringify(newBook),
    })
      .then((response) => response.json())
      .then((data) => {
        setIsLoading(false);
        props.onClose();
        props.onBookAdded();
        Swal.fire({
          icon: 'success',
          title: 'Book Added',
          text: 'The book has been added successfully!',
        });
      })
      .catch((error) => {
        console.log('Error adding book:', error);
        setIsLoading(false);
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: 'An error occurred while adding the book. Please try again later.',
        });
      });
  };


  return (
    <Modal show={props.show} onHide={props.onClose}>
      <Modal.Header closeButton>
        <Modal.Title style={{ fontFamily: "Gentium Book Plus", fontWeight: "bold" }}>Add New Book</Modal.Title>
      </Modal.Header>
      <Form onSubmit={handleSubmit}>
        <Modal.Body>
          <Form.Group controlId="formBasicName">
            <Form.Label style={{ fontFamily: "Gentium Book Plus", fontWeight: "bold" }}>Book Name</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter name"
              value={name}
              required
              onChange={(event) => setName(event.target.value)}
              style={{ fontFamily: "Gentium Book Plus" }}
            />
          </Form.Group>
          <Form.Group controlId="formBasicDescription">
            <Form.Label style={{ fontFamily: "Gentium Book Plus", fontWeight: "bold" }}>Description</Form.Label>
            <Form.Control
              as="textarea"
              rows={3}
              placeholder="Enter description"
              value={description}
              required
              onChange={(event) => setDescription(event.target.value)}
              style={{ fontFamily: "Gentium Book Plus" }}
            />
          </Form.Group>
          <Form.Group controlId="formBasicAuthor">
            <Form.Label style={{ fontFamily: "Gentium Book Plus", fontWeight: "bold" }}>Author</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter author"
              value={author}
              required
              onChange={(event) => setAuthor(event.target.value)}
              style={{ fontFamily: "Gentium Book Plus" }}
            />
          </Form.Group>
          <Form.Group controlId="formBasicGenre">
            <Form.Label style={{ fontFamily: "Gentium Book Plus", fontWeight: "bold" }}>Genre</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter genre"
              value={genre}
              required
              onChange={(event) => setGenre(event.target.value)}
              style={{ fontFamily: "Gentium Book Plus" }}
            />
          </Form.Group>
          <Form.Group controlId="formBasicImage">
            <Form.Label style={{ fontFamily: "Gentium Book Plus", fontWeight: "bold" }}>Image</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter image address"
              value={image}
              required
              onChange={(event) => setImage(event.target.value)}
              style={{ fontFamily: "Gentium Book Plus" }}
            />
          </Form.Group>
          <Form.Group controlId="formBasicPrice">
            <Form.Label style={{ fontFamily: "Gentium Book Plus", fontWeight: "bold" }}>Price</Form.Label>
            <Form.Control
              type="number"
              placeholder="Enter price"
              value={price}
              required
              onChange={(event) => setPrice(event.target.value)}
            />
          </Form.Group>
        </Modal.Body>
          <div style={{ display: 'flex' }}>
            <Button
              variant="success"
              type="submit"
              style={{
                fontFamily: 'Gentium Book Plus',
                fontSize: '20px',
                fontWeight: 'bold',
                flex: 1,
                borderRadius: '0 0 5px 5px'
              }}>
              {isLoading ? 'Adding Book...' : 'Add Book'}
            </Button>
          </div>
      </Form>
    </Modal>
  );

}
