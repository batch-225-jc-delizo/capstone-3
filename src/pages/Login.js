import { useState, useEffect, useContext } from 'react'
import { Form, Button, Card, Col, Row } from 'react-bootstrap'
import { useNavigate, Navigate, Link } from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'
import loginpage from '../assets/loginpage.png';
import readwell from '../assets/readwell.svg';
import '../App.css';

export default function Login() {
    // Initializes the use of the properties from the UserProvider in App.js file
    const { user, setUser } = useContext(UserContext)

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [isActive, setIsActive] = useState(false)

    const retrieveUser = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/booknook/users/details`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
            .then(response => response.json())
            .then(result => {

                // Store the user details retrieved from the token into the global user state
                setUser({
                    id: result._id,
                    isAdmin: result.isAdmin
                })
            })
    }


    function authenticate(event) {
        event.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/booknook/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
            .then(response => response.json())
            .then(result => {
                if (typeof result.access !== "undefined") {
                    localStorage.setItem('access', result.access)

                    retrieveUser(result.access)

                    Swal.fire({
                        title: 'Login Successful!',
                        icon: 'success',
                        text: 'Welcome to ReadWell!'
                    })
                } else {
                    Swal.fire({
                        title: 'Authentication Failed!',
                        icon: 'error',
                        text: 'Read a Book at a Nook'
                    })
                }
            })
    }




    useEffect(() => {
        if ((email !== '' && password !== '')) {
            // Enables the submit button if the form data has been verified
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [email, password])




    return (
        (user.id !== null) ?
            <Navigate to="/books" />
            :
            <>
                <div style={{ margin: 0, padding: 0 }}>
                    <Row>
                        <Col md={8} style={{ padding: 0 }}>
                            <img src={loginpage} className='login-sm-gone' alt="Login Page" style={{ height: '100vh', width: 'auto' }} />
                        </Col>
                        <Col md={4} className="login-page" style={{ padding: 0, backgroundColor: '#FFFEF5', paddingRight: '30px', paddingLeft: '30px' }}>
                            <div style={{ marginTop: '80px', display: 'block', justifyContent: 'center' }}>
                                <img src={readwell} alt="readwell" className='login-sm-gone' style={{ height: '30px' }} />
                            </div>
							<p className='login-sm-gone' style={{ maxWidth: '400px', marginTop: '20px', fontFamily: 'Roboto Slab', fontSize: '18px' }}>
                            "The more that you read, the more things you will know. The more that you learn, the more places you'll go." - Dr. Seuss
							</p>
                            <div className='login-padding'></div>
                            <div>
                                <Card className='login-center' style={{
                                    maxWidth: '400px',
                                    marginTop: '20px',
                                    backgroundColor: "#e2eafc",
                                    boxShadow: '0px 2px 4px rgba(0, 0, 0, 1)',
                                    borderRadius: '0'
                                }}>
                                    <Card.Body>
                                        <Form onSubmit={event => authenticate(event)}>
                                            <Form.Group controlId="userEmail" className='px-4 mt-3'>
                                                <Form.Control
                                                    type="email"
                                                    placeholder="Enter Email"
                                                    value={email}
                                                    onChange={event => setEmail(event.target.value)}
                                                    required
                                                    style={{
                                                        backgroundColor: '#edf2fb',
                                                        borderColor: '#edf2fb',
                                                        outline: 'none',
                                                        boxShadow: 'none',
                                                        fontFamily: 'Roboto Slab'
                                                    }}
                                                />
                                            </Form.Group>

                                            <Form.Group controlId="password" className='px-4 mt-1'>
                                                <Form.Control
                                                    type="password"
                                                    placeholder="Enter Password"
                                                    value={password}
                                                    onChange={event => setPassword(event.target.value)}
                                                    required
                                                    style={{
                                                        backgroundColor: '#edf2fb',
                                                        borderColor: '#edf2fb',
                                                        outline: 'none',
                                                        boxShadow: 'none',
                                                        fontFamily: 'Roboto Slab'
                                                    }}
                                                />
                                            </Form.Group>

                                            <div style={{ display: 'flex', justifyContent: 'center', marginTop: '10px' }} className='px-4'>
                                                {isActive ?
                                                    <Button variant="success" type="submit" id="submitBtn"
                                                        style={{
                                                            paddingLeft: '30px',
                                                            paddingRight: '30px',
                                                            fontFamily: 'Roboto Slab',
                                                            width: '100%'
                                                        }}>
                                                        LOG IN
                                                    </Button>
                                                    :
                                                    <Button variant="secondary" type="submit" id="submitBtn" disabled
                                                        style={{
                                                            paddingLeft: '30px',
                                                            paddingRight: '30px',
                                                            fontFamily: 'Roboto Slab',
                                                            width: '100%'
                                                        }}>
                                                        LOG IN
                                                    </Button>
                                                }
                                            </div>
                                            <div className="text-center mt-3 mb-3">
                                                <Form.Text style={{ color: 'black' }}>
                                                    Don’t have an account? <Link to="/register" style={{ textDecoration: 'none', color: '#0A58CA' }}>Sign up</Link>.
                                                </Form.Text>

                                            </div>

                                        </Form>
                                    </Card.Body>
                                </Card >
                            </div>
                            <p className='login-sm-gone' style={{ maxWidth: '400px', marginTop: '30px', fontFamily: 'Roboto Slab', fontSize: '18px' }}>
                                "A reader lives a thousand lives before he dies. <br/>The man who never reads lives only one." <br/>- George R.R. Martin
                            </p>
                        </Col>
                    </Row>
                </div>
            </>
    )
}
