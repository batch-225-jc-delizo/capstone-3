import { useEffect, useState } from "react";
import { Card, Container, Button, Col, Row } from "react-bootstrap";
import { useNavigate } from 'react-router-dom';
import emptybag from '../assets/emptybag.svg';
import Swal from "sweetalert2";

export default function Orders() {
  const [orders, setOrders] = useState([]);
  const [forceRefresh, setForceRefresh] = useState(false);
  const navigate = useNavigate();
  const accessToken = localStorage.getItem('access');

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/booknook/order/retrieve-a-user-order`, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${accessToken}`
      }
    })
      .then((res) => res.json())
      .then(async (data) => {
        // Loop through each order
        const updatedOrders = await Promise.all(data.map(async (order) => {
          // Loop through each book in the order and get its details
          const updatedBooks = await Promise.all(order.books.map(async (bookItem) => {
            const response = await fetch(`${process.env.REACT_APP_API_URL}/booknook/books/specificbook/${bookItem.book}`, {
              method: 'GET',
              headers: {
                Authorization: `Bearer ${accessToken}`
              }
            });
            const data = await response.json();
            return {
              ...bookItem,
              book: data
            };
          }));
          // Update the order with the book details
          return {
            ...order,
            books: updatedBooks
          };
        }));
        // Set the state with the updated orders
        setOrders(updatedOrders);
      })
      .catch((error) => console.log(error));
  }, [accessToken, forceRefresh]);

  const handleFillUpClick = () => {
    navigate('/books');
  }

  const handleCancelOrder = async (orderId) => {
    try {
      // Show confirmation modal
      const result = await Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#d33',
        cancelButtonColor: '#3085d6',
        confirmButtonText: 'Yes, cancel it!'
      });
      if (result.isConfirmed) {
        await fetch(`${process.env.REACT_APP_API_URL}/booknook/order/delete-order/${orderId}`, {
          method: 'DELETE',
          headers: {
            Authorization: `Bearer ${accessToken}`
          }
        });

        // Show success message
        await Swal.fire({
          title: 'Order cancelled!',
          text: 'Your order has been cancelled.',
          icon: 'success'
        });

        // Set the state with the updated orders
        setForceRefresh(!forceRefresh);
      }
    } catch (error) {
      console.log(error);
    }
  }

  return (
    <Container style={{ paddingTop: '80px', paddingBottom: '10px' }}>
      {orders.length > 0 ? (
        orders.slice().reverse().map((order) => (
          <Card key={order._id} style={{ marginBottom: '10px', borderRadius: 0, border: 'none' }}>
            <Card.Body>
              <Row>
                <Col xs={12} md={7}>
                  <div>
                    {order.books.map((bookItem, index) => (
                      <Card key={bookItem.book._id} className="mb-2">
                        <Card.Body>
                          <Row>
                            <Col xs={4} sm={4} md={3} lg={2} className="text-center">
                              <Card.Img src={bookItem.book.image} alt={bookItem.book.name} style={{ height: '100%', objectFit: 'cover', cursor: 'pointer' }} />
                            </Col>
                            <Col xs={8} sm={8} md={9} lg={10}>
                              <Card.Title>{bookItem.book.name} by {bookItem.book.author}</Card.Title>
                              <div>Qty: {bookItem.quantity}</div>
                              <div>₱{bookItem.book.price}.00</div>
                            </Col>
                          </Row>
                        </Card.Body>
                      </Card>
                    ))}
                  </div>
                </Col>
                <Col xs={12} md={5}>
                  <div style={{ color: '#4CAF50', fontWeight: 'bold', marginBottom: '5px' }}>
                    Get by {new Date(Date.now() + 3 * 24 * 60 * 60 * 1000).toLocaleDateString('en-US', { month: 'short', day: 'numeric' })}-
                    {new Date(Date.now() + 5 * 24 * 60 * 60 * 1000) > new Date(order.createdOn + 4 * 24 * 60 * 60 * 1000) ? "Received" :
                      new Date(Date.now() + 5 * 24 * 60 * 60 * 1000).toLocaleDateString('en-US', { month: 'short', day: 'numeric' })}
                  </div>
                  <Card.Subtitle className="mb-3 text-muted">{new Date(order.createdOn).toLocaleDateString()}</Card.Subtitle>
                  <div>
                    <div>Shipping Address: <b>{order.shippingAddress}</b></div>
                    <div>Shipping Fee: <b>₱{order.shippingFee}.00</b></div>
                    <div>Total Price: <b>₱{order.totalPrice}.00</b></div>
                    <div>Payment Method: <b>{order.modeOfPayment}</b></div>
                  </div>

                  {new Date(order.createdOn) >= new Date(Date.now() - 12 * 60 * 60 * 1000) && (
                    <div className="mt-4">
                      <button
                        className="btn btn-secondary"
                        onClick={() => handleCancelOrder(order._id)}
                        style={{ borderRadius: 0 }}
                      >
                        Cancel Order
                      </button>
                    </div>
                  )}
                  <div style={{ marginTop: '5px' }}>
                    <p style={{ fontSize: '12px' }}>Please note that cancellation is only allowed within <b>12 hours</b> after placing your order. After this period, cancellation will no longer be accepted.</p>
                  </div>
                </Col>
              </Row>
            </Card.Body>
          </Card>
        ))
      ) : (
        <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', height: '50vh' }}>
          <h2 style={{ color: '#555', textAlign: 'center' }}>You haven't ordered anything yet!</h2>
          <img src={emptybag} alt="empty cart" style={{ width: '200px', marginBottom: '20px' }} />
          <span className="fill-up-text" onClick={handleFillUpClick}>Fill me up, buttercup!</span>
        </div>
      )}
    </Container>

  );
}  