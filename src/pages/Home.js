import React, { useEffect, useState } from "react";
import { Container, Row, Col, Card, Button } from "react-bootstrap";
import { useNavigate } from 'react-router-dom';
import Loading from '../components/Loading';
import '../App.css';
import { FaFacebook, FaTwitter, FaGooglePlus, FaInstagram, FaYoutube, FaPinterest, FaTumblr, FaFedex, FaUps, FaUsps, FaDhl, FaRegCopyright } from 'react-icons/fa';

const HomePage = () => {
  const [books, setBooks] = useState([]);
  const [newBooks, setNewBooks] = useState([]);
  const [topBooks, setTopBooks] = useState([]);
  const [loading, setLoading] = useState(true);
  const navigate = useNavigate();

  useEffect(() => {
    const fetchBooks = async () => {
      const ids = [
        "6404a3b481f62b7ab2d249e8",
        "644cecd2d30de2ab90be2899",
        "644ced2ed30de2ab90be28a0",
        "644085ca0f48fc5108bc2ca1",
      ];

      const promises = ids.map((id) =>
        fetch(`${process.env.REACT_APP_API_URL}/booknook/books/specificbook/${id}`)
          .then((res) => res.json())
      );

      const books = await Promise.all(promises);
      setBooks(books);
      setLoading(false);
    };

    fetchBooks();
  }, []);

  useEffect(() => {
    const fetchTopBooks = async () => {
      const topBooksIds = [
        "6404a2f981f62b7ab2d249de",
        "6404a3b481f62b7ab2d249e8",
        "6404a4b281f62b7ab2d249fa",
        "6404a53081f62b7ab2d24a01",
      ];

      const promises = topBooksIds.map((id) =>
        fetch(`${process.env.REACT_APP_API_URL}/booknook/books/specificbook/${id}`)
          .then((res) => res.json())
      );

      const topBooks = await Promise.all(promises);
      setTopBooks(topBooks);
    };

    fetchTopBooks();
    setLoading(false);
  }, []);

  useEffect(() => {
    const fetchNewBooks = async () => {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/booknook/books/activebooks`);
      const newBooks = await response.json();
      setNewBooks(newBooks.reverse().slice(0, 4));
    };

    fetchNewBooks();
    setLoading(false);
  }, []);

  if (loading) {
    return <Loading />;
  }

  return (
    <>
      <Container style={{ paddingTop: '80px' }}>
        <Row>
          <Col md={{ span: 8, offset: 2 }} className="text-center">
            <p className="lead" style={{ fontFamily: 'Gentium Book Plus', fontWeight: 'bold' }}>Explore bookshelves and find your next favorite read!</p>
          </Col>
        </Row>
        <Row className="mb-3">
          <Col md={{ span: 8, offset: 2 }} className="text-center">
            <h2 style={{ fontFamily: 'Gentium Book Plus', fontWeight: 'bold' }}>FEATURED BOOKS</h2>
          </Col>
        </Row>
        <Row style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
          {books.map(({ _id, author, image, description }) => (
            <Col key={_id}>
              <Card onClick={() => navigate(`/bookpage/${_id}`)} className='hoverme'
                style={{
                  maxWidth: '300px',
                  marginLeft: '2px',
                  marginRight: '2px',
                  marginTop: '10px',
                  boxShadow: '0px 4px 8px rgba(0, 0, 0, 0.15)',
                  transition: 'transform 0.2s ease-in-out',
                  cursor: 'pointer',
                  borderRadius: 0
                }}>
                <Card.Img variant="top" src={image} fluid="true" style={{ height: '200px', objectFit: 'cover', cursor: 'pointer', borderRadius: 0 }} />
                <Card.Body className="home-featurecard">
                  <Card.Text style={{
                    WebkitLineClamp: '3',
                    overflow: 'hidden',
                    textOverflow: 'ellipsis',
                    display: '-webkit-box',
                    WebkitBoxOrient: 'vertical',
                    fontFamily: 'Gentium Book Plus'
                  }}>
                    {description}
                  </Card.Text>
                </Card.Body>
              </Card>
            </Col>
          ))}
        </Row>
        <Row className="mb-3 mt-5">
          <Col md={{ span: 8, offset: 2 }} className="text-center">
            <h2 style={{ fontFamily: 'Gentium Book Plus', fontWeight: 'bold' }}>NEW ARRIVALS</h2>
          </Col>
        </Row>
        <Row style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
          {newBooks.map(({ _id, author, image, description }) => (
            <Col key={_id}>
              <Card onClick={() => navigate(`/bookpage/${_id}`)} className='hoverme'
                style={{
                  maxWidth: '300px',
                  marginLeft: '2px',
                  marginRight: '2px',
                  marginTop: '10px',
                  boxShadow: '0px 4px 8px rgba(0, 0, 0, 0.15)',
                  transition: 'transform 0.2s ease-in-out',
                  cursor: 'pointer',
                  borderRadius: 0
                }}>
                <Card.Img variant="top" src={image} fluid="true" style={{ height: '200px', objectFit: 'cover', cursor: 'pointer', borderRadius: 0 }} />
                <Card.Body className="home-featurecard">
                  <Card.Text style={{
                    WebkitLineClamp: '5',
                    overflow: 'hidden',
                    textOverflow: 'ellipsis',
                    display: '-webkit-box',
                    WebkitBoxOrient: 'vertical',
                    fontFamily: 'Gentium Book Plus'
                  }}>
                    {description}
                  </Card.Text>
                </Card.Body>
              </Card>
            </Col>
          ))}
        </Row>
        <Row className="mb-3 mt-5">
          <Col md={{ span: 8, offset: 2 }} className="text-center">
            <h2 style={{ fontFamily: 'Gentium Book Plus', fontWeight: 'bold' }}>MOST POPULAR</h2>
          </Col>
        </Row>
        <Row style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
          {topBooks.map(({ _id, image, description }) => (
            <Col key={_id}>
              <Card onClick={() => navigate(`/bookpage/${_id}`)} className='hoverme'
                style={{
                  maxWidth: '300px',
                  marginLeft: '2px',
                  marginRight: '2px',
                  marginTop: '10px',
                  boxShadow: '0px 4px 8px rgba(0, 0, 0, 0.15)',
                  transition: 'transform 0.2s ease-in-out',
                  cursor: 'pointer',
                  borderRadius: 0
                }}>
                <Card.Img variant="top" src={image} fluid="true" style={{ height: '200px', objectFit: 'cover', cursor: 'pointer', borderRadius: 0 }} />
                <Card.Body className="home-featurecard">
                  <Card.Text style={{
                    WebkitLineClamp: '5',
                    overflow: 'hidden',
                    textOverflow: 'ellipsis',
                    display: '-webkit-box',
                    WebkitBoxOrient: 'vertical',
                    fontFamily: 'Gentium Book Plus'
                  }}>
                    {description}
                  </Card.Text>
                </Card.Body>
              </Card>
            </Col>
          ))}
        </Row>
      </Container>
      <Container style={{ backgroundColor: '#F4F4F6', maxWidth: '100vw', marginLeft: 0, marginRight: 0 }}>
        <Row style={{ padding: '50px', marginTop: '50px' }}>
          <Col md={4}>
            <h5 style={{ fontSize: '13px' }}>The Top Online Book Shopping Experience with READWELL Philippines</h5>
            <p style={{ fontSize: '11px' }}>
              As the top online shopping platform in the Philippines, ReadWell continues to give its contributions to the growing e-commerce community in the country, creating several opportunities for brands and sellers to grow in their business, while also promoting their items online. At the same time, Filipinos are given new ways to discover and access the best products for themselves. Ordering legit and top-quality items online, fast and free delivery, and responsive logistics and customer services, all of these are continuously honed and perfected by ReadWell using the company's decade-long experience in their quality service, constantly expanding the assortment of products and offering convenient payment options and delivery anywhere in the country. The true effortless shopping experience always begins with ReadWell, since everything you want, whether it is fast service or the best products at the lowest prices.
            </p>
          </Col>
          <Col md={4}>
            <h5 style={{ fontSize: '13px' }}>READWELL PAYMENT CONVENIENCE</h5>
            <p style={{ fontSize: '11px' }}>
              Online shopping and transactions are made simpler as ReadWell offers you convenient ways to pay for your orders online. Aside from the trusted Cash on Delivery option, ReadWell will also accepts Credit Card payments soon through its secured online transaction records. Customers can also enjoy seamless transactions through the ReadWallet function. This digital wallet will let you load online credits and use them to effortlessly pay for the items you want online. In the future, you can also avail of other added vouchers, deals, and even cashback through ReadWallet, giving you more ways to save more when buying through ReadWell. Finally, the platform will also introduce the ReadPayLater option for those who want to shop now and pay later through reasonable installments.
            </p>
          </Col>
          <Col md={4}>
            <h5 style={{ fontSize: '13px' }}>AUTHENTIC PRODUCT GUARANTEED</h5>
            <p style={{ fontSize: '11px' }}>
              At ReadWell, we take pride in offering a collection of books that are truly authentic and original. We believe that reading is not just a pastime but a way of life, and therefore, we are committed to providing our customers with books that are unique and inspiring. Our books are carefully curated by experts who are passionate about literature, ensuring that each one is a masterpiece in its own right. Whether you're looking for a classic novel, a gripping thriller, or a thought-provoking non-fiction work, you can be sure that our books are of the highest quality and are guaranteed to provide you with an unforgettable reading experience. We believe that our commitment to authenticity and originality sets us apart from other bookstores, and we look forward to sharing our love of literature with you.
            </p>
          </Col>
        </Row>
        <Row style={{ paddingRight: '50px', paddingLeft: '50px', paddingBottom: '50px' }}>
          <Col md={4} className="text-start mb-3">
            <h5 style={{ fontSize: '15px' }}>Follow Us</h5>
            <div style={{ fontSize: '25px' }}>
              <a href="https://www.facebook.com/" target="_blank" rel="noreferrer"><FaFacebook style={{ color: "gray", margin: '2px' }} /></a>
              <a href="https://twitter.com/" target="_blank" rel="noreferrer"><FaTwitter style={{ color: "gray", margin: '2px' }} /></a>
              <a href="https://plus.google.com/" target="_blank" rel="noreferrer"><FaGooglePlus style={{ color: "gray", margin: '2px' }} /></a>
              <a href="https://www.instagram.com/" target="_blank" rel="noreferrer"><FaInstagram style={{ color: "gray", margin: '2px' }} /></a>
              <a href="https://www.youtube.com/" target="_blank" rel="noreferrer"><FaYoutube style={{ color: "gray", margin: '2px' }} /></a>
              <a href="https://www.pinterest.com/" target="_blank" rel="noreferrer"><FaPinterest style={{ color: "gray", margin: '2px' }} /></a>
              <a href="https://www.tumblr.com/" target="_blank" rel="noreferrer"><FaTumblr style={{ color: "gray", margin: '2px' }} /></a>
            </div>
          </Col>
          <Col md={4} className="text-center">
            <h5 className="text-muted" style={{ fontSize: '15px', fontFamily: 'Gentium Book Plus'}}><FaRegCopyright /> READWELL 2023</h5>
            <h5 className="text-muted" style={{ fontSize: '15px', fontFamily: 'Gentium Book Plus'}}>JC Delizo</h5>
          </Col>
          <Col md={4} className="text-end">
            <h5 style={{ fontSize: '15px' }}>Delivery Partners</h5>
            <div style={{ fontSize: '30px' }}>
              <a href="https://www.fedex.com/" target="_blank" rel="noreferrer"><FaFedex style={{ color: "gray", margin: '2px' }} /></a>
              <a href="https://www.ups.com/" target="_blank" rel="noreferrer"><FaUps style={{ color: "gray", margin: '2px' }} /></a>
              <a href="https://www.usps.com/" target="_blank" rel="noreferrer"><FaUsps style={{ color: "gray", margin: '2px' }} /></a>
              <a href="https://www.dhl.com/" target="_blank" rel="noreferrer"><FaDhl style={{ color: "gray", margin: '2px' }} /></a>
            </div>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default HomePage;
