import React, { useState, useEffect } from "react";
import { Table, Button, DropdownButton, Dropdown, Card, Row, Col, Container } from "react-bootstrap";

const ViewOrders = () => {
  const [orders, setOrders] = useState([]);
  const [users, setUsers] = useState({});
  const [books, setBooks] = useState({});
  const accessToken = localStorage.getItem('access');

  useEffect(() => {
    let url = `${process.env.REACT_APP_API_URL}/booknook/order/get-orders`;
    fetch(url, {
      headers: {
        Authorization: `Bearer ${accessToken}`
      }
    })
      .then((res) => res.json())
      .then((data) => setOrders(data))
      .catch((err) => console.log(err));
  }, [accessToken]);
  

  useEffect(() => {
    const getUserDetails = async (userId) => {
      const res = await fetch(
        `${process.env.REACT_APP_API_URL}/booknook/users/whosthisuser/${userId}`, {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${accessToken}`
        },
      }
      );
      const data = await res.json();
      setUsers((prevState) => ({
        ...prevState,
        [userId]: data,
      }));
    };

    orders.forEach((order) => {
      if (!users[order.user]) {
        getUserDetails(order.user);
      }
      order.books.forEach(async (book) => {
        if (!books[book.book]) {
          const res = await fetch(`${process.env.REACT_APP_API_URL}/booknook/books/specificbook/${book.book}`, {
            method: 'GET',
            headers: {
              Authorization: `Bearer ${accessToken}`
            },
          });
          const data = await res.json();
          setBooks((prevState) => ({
            ...prevState,
            [book.book]: data.name,
          }));
        }
      });
    });
  }, [orders]);
  


  return (
    <div style={{ paddingTop: '80px', paddingBottom: '80px' }}>
      <div style={{ display: 'flex', justifyContent: 'flex-end', alignItems: 'flex-end'}} className="mb-3 mx-3 mx-md-5" >
      </div>
      <Container>
        <Row>
          {orders
            .sort((a, b) => new Date(b.createdOn) - new Date(a.createdOn))
            .map((order) => (
              <Col key={order._id} xs={12} md={6} lg={4}>
                <Card bg={order.isDelivered ? "light" : "secondary"} text={order.isDelivered ? "black" : "white"} className="my-1" style={{ border: 'none' }}>
                  <Card.Body>
                    <Card.Title>{users[order.user] ? users[order.user].name : "Loading..."}</Card.Title>
                    <Card.Subtitle className="mb-2">Order ID: {order._id}</Card.Subtitle>
                    <Card.Text><b>{new Date(order.createdOn).toLocaleString()}</b></Card.Text>
                    <Card.Text style={{ fontWeight: 'bold' }}>
                      {order.books.map((book) => (
                        <div key={book._id}>
                          {book.quantity}x{" "}
                          {books[book.book] ? books[book.book] : "Loading..."}
                        </div>
                      ))}
                    </Card.Text>
                    <Card.Text>Total Price: <b>₱{order.totalPrice}.00</b></Card.Text>
                    <Card.Text>Shipping Address: <b>{order.shippingAddress}</b></Card.Text>
                    <Card.Text>Mode of Payment: <b>{order.modeOfPayment}</b></Card.Text>
                  </Card.Body>
                  <Button
                    variant={order.isDelivered ? 'success' : 'warning'}
                    style={{ fontWeight: 'bold', borderRadius: '0 0 5px 5px' }}
                    onClick={() => {
                      const url = `${process.env.REACT_APP_API_URL}/booknook/order/order-delivered/${order._id}`;
                      const method = "PUT";
                      const headers = {
                        "Content-Type": "application/json",
                        Authorization: `Bearer ${accessToken}`,
                      };
                      const body = JSON.stringify({
                        isDelivered: !order.isDelivered,
                      });
                      fetch(url, { method, headers, body })
                        .then((res) => res.json())
                        .then((data) => {
                          const updatedOrders = orders.map((o) =>
                            o._id === data._id ? data : o
                          );
                          setOrders(updatedOrders);
                        })
                        .catch((err) => console.log(err));
                    }}
                  >
                    {order.isDelivered ? "Sent" : "Pending"}
                  </Button>
                </Card>
              </Col>
            ))}
        </Row>
      </Container>
    </div>

  );
}
export default ViewOrders