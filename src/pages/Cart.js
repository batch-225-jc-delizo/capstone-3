import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { Form, Button, Table, Col, Row, Card } from 'react-bootstrap';
import Swal from 'sweetalert2';
import emptycart from '../assets/emptycart.svg';
import '../App.css';

export default function Cart() {
  const [cart, setCart] = useState(null);
  const [shippingAddress, setShippingAddress] = useState('');
  const [paymentMode, setPaymentMode] = useState('');
  const [forceRerender, setForceRerender] = useState(false);
  const navigate = useNavigate();
  const shippingFee = 100;
  const accessToken = localStorage.getItem('access');

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/booknook/cart/view-cart`, {
      headers: {
        Authorization: `Bearer ${accessToken}`
      }
    })
      .then(response => response.json())
      .then(data => setCart(data))
      .catch(error => console.log(error));
  }, [forceRerender]);

  const handleAddressChange = (e) => {
    setShippingAddress(e.target.value);
  };

  const handlePaymentModeChange = (e) => {
    setPaymentMode(e.target.value);
  };

  const handleCheckout = () => {
    const checkoutData = {
      cartId: cart._id,
      shippingAddress,
      paymentMode
    };

    fetch(`${process.env.REACT_APP_API_URL}/booknook/order/checkout`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${accessToken}`
      },
      body: JSON.stringify(checkoutData)
    })
      .then(response => {
        if (!response.ok) {
          throw new Error('Checkout failed');
        }
        return response.json();
      })
      .then(() => {
        Swal.fire({
          icon: 'success',
          title: 'Checkout successful!',
          showCancelButton: true,
          confirmButtonText: 'View Orders',
          cancelButtonText: 'Continue Shopping'
        }).then((result) => {
          if (result.isConfirmed) {
            navigate('/orders');
          } else {
            navigate('/books');
            setForceRerender(!forceRerender);
          }
        });
      })
      .catch(error => {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: error.message
        });
      });
  };

  const handleFillUpClick = () => {
    navigate('/books');
  }

  const updateQuantity = (bookId, quantity) => {
    console.log(`updateQuantity called with bookId ${bookId} and quantity ${quantity}`);
    const bookToUpdate = cart.books.find(book => book._id === bookId);
    if (!bookToUpdate) {
      return;
    }

    const updateData = {
      bookId: bookToUpdate.book._id,
      quantity
    };

    fetch(`${process.env.REACT_APP_API_URL}/booknook/cart/update-quantities/${bookToUpdate.book._id}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${accessToken}`
      },
      body: JSON.stringify(updateData)
    })
      .then(response => {
        if (!response.ok) {
          throw new Error('Update failed');
        }
        return response.json();
      })
      .then(() => {
        setForceRerender(!forceRerender);
      })
      .catch(error => {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: error.message
        });
      });
  };



  return (
    <div style={{ paddingTop: '80px' }}>
      {!cart || isNaN(cart.subtotal) || (cart.subtotal + shippingFee === 100) ? (
        <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', height: '50vh' }}>
          <h2 style={{ color: '#555', textAlign: 'center' }}>No items in the cart.</h2>
          <img src={emptycart} alt="empty cart" style={{ width: '200px', marginBottom: '5px' }} />
          <span className="fill-up-text" onClick={handleFillUpClick}>Fill me up, buttercup!</span>
        </div>
      ) : (
        <div>
          <Row style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'space-around', alignItems: 'flex-start' }}>
            <Col md={8}>
              <div style={{ marginBottom: '20px' }}>
                {cart.books.map((book) => (
                  <Card key={book._id} style={{ marginBottom: '8px', border: 'none' }}>
                    <Row>
                      <Col md={4}>
                        <Card.Img src={book.book.image} style={{ maxHeight: '165px', objectFit: 'cover', cursor: 'pointer', borderRadius: '0' }} />
                      </Col>
                      <Col md={8}>
                        <Row style={{ display: 'flex', flexWrap: 'wrap' }}>
                          <Col xs={5} md={6}>
                            <Card.Body>
                              <Card.Title className='m-auto'
                                style={{
                                  fontFamily: "Gentium Book Plus",
                                  fontWeight: 'bold',
                                  fontSize: '25px'
                                }}
                              >
                                {book.book.name}
                              </Card.Title>
                              <Card.Text className='m-auto'
                                style={{
                                  fontFamily: "Gentium Book Plus",
                                  fontWeight: 'bold',
                                  fontSize: '15px'
                                }}
                              >
                                {book.book.author}
                              </Card.Text>
                              <Card.Text className='m-auto'
                                style={{
                                  fontFamily: "Gentium Book Plus",
                                  fontWeight: 'bold',
                                  fontSize: '15px'
                                }}
                              >
                                {book.book.genre}
                              </Card.Text>
                            </Card.Body>
                          </Col>
                          <Col xs={3} md={3}>
                            <Card.Text
                              style={{
                                fontFamily: 'Roboto',
                                fontWeight: 'bold',
                                fontSize: '20px',
                                color: '#F78124',
                                marginTop: '20px',
                                marginLeft: '20px',
                              }}
                            >
                              ₱{book.book.price}
                            </Card.Text>
                          </Col>
                          <Col xs={4} md={3}>
                            <div style={{ display: "inline-block", marginTop: '20px', marginBottom: '20px', marginLeft: '20px' }}>
                              <Button
                                variant="danger"
                                size="sm"
                                onClick={() => updateQuantity(book._id, book.quantity - 1)}
                                style={{ borderRadius: 0, }}
                              >
                                -
                              </Button>
                              <span style={{ margin: "0 10px", }}>
                                {book.quantity}
                              </span>
                              <Button
                                variant="success"
                                size="sm"
                                onClick={() => updateQuantity(book._id, book.quantity + 1)}
                                style={{ borderRadius: 0 }}
                              >
                                +
                              </Button>
                            </div>
                          </Col>
                        </Row>
                      </Col>

                    </Row>
                  </Card>
                ))}
              </div>
            </Col>

            <Col md={3} style={{ backgroundColor: '#fff' }}>
              <Form style={{ padding: '20px' }}>
                <Form.Group controlId="formShippingAddress">
                  <Form.Label>
                    <span style={{ fontWeight: 'bold' }}>Location</span>
                    <i className="bi bi-geo-alt-fill" style={{ marginLeft: '5px' }}></i>
                  </Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Enter your shipping address"
                    onChange={handleAddressChange}
                  />
                  <Form.Label style={{ fontWeight: 'bold', marginTop: '10px' }}>Payment Method</Form.Label>
                  <Form.Select
                    aria-label="Payment Method"
                    value={paymentMode}
                    onChange={handlePaymentModeChange}
                  >
                    <option value="">Select payment method</option>
                    <option value="COD">Cash on Delivery</option>
                    <option value="paypal">Paypal (Coming Soon)</option>
                    <option value="gcash">GCash (Coming Soon)</option>
                  </Form.Select>
                </Form.Group>

                <div style={{ marginTop: '20px' }}>
                  <h5 style={{ fontWeight: 'bold' }}>Order Summary</h5>

                  <Table borderless>
                    <tbody>
                      <tr>
                        <td>Subtotal of the items:</td>
                        <td style={{ fontWeight: 'bold' }}>₱{cart.subtotal}.00</td>
                      </tr>
                      <tr>
                        <td>Shipping Fee:</td>
                        <td style={{ fontWeight: 'bold' }}>₱100.00</td>
                      </tr>
                      <tr>
                        <td>Total:</td>
                        <td style={{ fontWeight: 'bold', fontFamily: 'Roboto', color: '#F78124', fontSize: '20px' }}>₱{cart.subtotal + 100}.00</td>
                      </tr>
                    </tbody>
                  </Table>

                  <Button
                    variant="primary"
                    onClick={handleCheckout}
                    className="darkhover"
                    disabled={!cart.books.length || !shippingAddress || !paymentMode}
                    style={{
                      marginTop: '20px',
                      width: '100%',
                      borderRadius: '0',
                      backgroundColor: '#F78124',
                      borderColor: '#F78124',
                      fontWeight: 'bold',
                      fontFamily: 'Roboto',
                    }}
                    activestyle={{
                      backgroundColor: '#FABE9B',
                      borderColor: '#FABE9B',
                      fontWeight: 'bold',
                      fontFamily: 'Roboto',
                    }}
                  >
                    Checkout
                  </Button>
                </div>
              </Form>
            </Col>

          </Row>
        </div>
      )}
    </div>

  );
}  