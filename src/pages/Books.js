import BookCard from '../components/BookCard';
import Loading from '../components/Loading';
import { useEffect, useState } from 'react';

export default function Books() {
  const [books, setBooks] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [selectedGenre, setSelectedGenre] = useState(null);
  const [searchTerm, setSearchTerm] = useState('');

  // useEffect(() => {
  //   fetch(`${process.env.REACT_APP_API_URL}/booknook/books/activebooks`)
  //     .then(response => response.json())
  //     .then(result => {
  //       const activeBooks = result.filter(book => book.isActive === true);
  //       setBooks(activeBooks);
  //       setIsLoading(false);
  //     });
  // }, []);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/booknook/books/activebooks`)
      .then(response => response.json())
      .then(result => {
        const activeBooks = result.filter(book => book.isActive === true);
        activeBooks.reverse(); // Reverse the order of elements
        setBooks(activeBooks);
        setIsLoading(false);
      });
  }, []);
  

  // useEffect(() => {
  //   fetch(`${process.env.REACT_APP_API_URL}/booknook/books/activebooks`)
  //     .then(response => response.json())
  //     .then(result => {
  //       const activeBooks = result.filter(book => book.isActive === true);
  //       // shuffle the activeBooks array using Fisher-Yates shuffle algorithm
  //       for (let i = activeBooks.length - 1; i > 0; i--) {
  //         const j = Math.floor(Math.random() * (i + 1));
  //         [activeBooks[i], activeBooks[j]] = [activeBooks[j], activeBooks[i]];
  //       }
  //       setBooks(activeBooks);
  //       setIsLoading(false);
  //     });
  // }, []);
  

  const handleGenreSelect = genre => {
    setSelectedGenre(genre);
  };

  const filteredBooks = selectedGenre
    ? books.filter(book => book.genre === selectedGenre)
    : books;
  const searchedBooks = searchTerm
    ? filteredBooks.filter(book =>
      book.name.toLowerCase().includes(searchTerm.toLowerCase())
    )
    : filteredBooks;

    return (
      <>
        <div style={{ maxWidth: '100%', marginLeft: 0, marginRight: 0, paddingTop: '80px' }}>
          <div className="row" style={{ maxWidth: '100%', marginLeft: 0, marginRight: 0 }}>
            <div className="col-md-2 position-md-fixed mb-3">
              <div className="input-group mb-3">
                <input
                  type="text"
                  className="form-control"
                  placeholder="Search books"
                  value={searchTerm}
                  onChange={(e) => setSearchTerm(e.target.value)}
                  style={{ fontFamily: 'Gentium Book Plus' }}
                />
              </div>
              <button
                className="btn btn-secondary w-100 mb-3 d-md-none"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#filterCollapse"
                aria-expanded="false"
                aria-controls="filterCollapse"
                style={{ fontFamily: 'Gentium Book Plus', fontWeight: 'bold', fontSize: '18px' }}
              >
                Genres
              </button>
              <div className="collapse d-md-block position-sticky top-0" id="filterCollapse">
                <div className="d-grid gap-2">
                  <button
                    className={`btn btn-outline-success ${
                      selectedGenre === null ? 'active' : ''
                    }`}
                    onClick={() => handleGenreSelect(null)}
                    style={{ fontFamily: 'Gentium Book Plus', fontWeight: 'bold' }}
                  >
                    All Books
                  </button>
                  <button
                    className={`btn btn-outline-secondary ${
                      selectedGenre === 'Fiction' ? 'active' : ''
                    }`}
                    onClick={() => handleGenreSelect('Fiction')}
                    style={{ fontFamily: 'Gentium Book Plus', fontWeight: 'bold' }}
                  >
                    Fiction
                  </button>
                  <button
                    className={`btn btn-outline-secondary ${
                      selectedGenre === 'Non-fiction' ? 'active' : ''
                    }`}
                    onClick={() => handleGenreSelect('Non-fiction')}
                    style={{ fontFamily: 'Gentium Book Plus', fontWeight: 'bold' }}
                  >
                    Non-Fiction
                  </button>
                  <button
                    className={`btn btn-outline-secondary ${
                      selectedGenre === 'Ancient Mythology' ? 'active' : ''
                    }`}
                    onClick={() => handleGenreSelect('Ancient Mythology')}
                    style={{ fontFamily: 'Gentium Book Plus', fontWeight: 'bold' }}
                  >
                    Ancient Mythology
                  </button>
                  <button
                    className={`btn btn-outline-secondary ${
                      selectedGenre === 'Romance' ? 'active' : ''
                    }`}
                    onClick={() => handleGenreSelect('Romance')}
                    style={{ fontFamily: 'Gentium Book Plus', fontWeight: 'bold' }}
                  >
                    Romance
                  </button>
                  <button
                    className={`btn btn-outline-secondary ${
                      selectedGenre === 'Mystery' ? 'active' : ''
                    }`}
                    onClick={() => handleGenreSelect('Mystery')}
                    style={{ fontFamily: 'Gentium Book Plus', fontWeight: 'bold' }}
                  >
                    Mystery
                  </button>
                  <button
                    className={`btn btn-outline-secondary ${
                      selectedGenre === 'Programming' ? 'active' : ''
                    }`}
                    onClick={() => handleGenreSelect('Programming')}
                    style={{ fontFamily: 'Gentium Book Plus', fontWeight: 'bold' }}
                  >
                    Programming
                  </button>
                  <button
                    className={`btn btn-outline-secondary ${
                      selectedGenre === 'Personal Finance' ? 'active' : ''
                    }`}
                    onClick={() => handleGenreSelect('Personal Finance')}
                    style={{ fontFamily: 'Gentium Book Plus', fontWeight: 'bold' }}
                  >
                    Personal Finance
                  </button>
                  <button
                    className={`btn btn-outline-secondary ${
                      selectedGenre === 'Self-Development' ? 'active' : ''
                    }`}
                    onClick={() => handleGenreSelect('Self-Development')}
                    style={{ fontFamily: 'Gentium Book Plus', fontWeight: 'bold' }}
                  >
                    Self-Development
                  </button>
                </div>
              </div>
            </div>
    
            <div className="col-md-10 mt-md-2 pt-md-0 mb-5 flex-wrap" style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
              {isLoading ? (
                <Loading />
              ) : searchedBooks.length > 0 ? (
                searchedBooks.map((book) => (
                  <BookCard key={book._id} bookProp={book} />
                ))
              ) : (
                <p style={{ fontFamily: 'Gentium Book Plus', fontWeight: 'bold', fontSize: '30px' }}>No books found.</p>
              )}
            </div>
          </div>
        </div>
      </>
    );
    
    

}
