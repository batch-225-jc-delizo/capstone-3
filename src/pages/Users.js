import React, { useState, useEffect } from 'react';
import { Card, Container, Row, Col, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import '../App.css';

function ViewUsers() {
    const [users, setUsers] = useState([]);
    const [searchQuery, setSearchQuery] = useState('');
    const [accessToken, setAccessToken] = useState(localStorage.getItem('access'));

    useEffect(() => {
        const fetchUsers = async () => {
            const response = await fetch(`${process.env.REACT_APP_API_URL}/booknook/users/all`, {
                method: 'GET',
                headers: {
                    Authorization: `Bearer ${accessToken}`
                },
            });
            const data = await response.json();
            setUsers(data);
        };

        fetchUsers();
    }, []);

    const handlePromoteClick = async (userId) => {
        const response = await fetch(`${process.env.REACT_APP_API_URL}/booknook/users/makeadmin/${userId}`, {
            method: 'PUT',
            headers: {
                Authorization: `Bearer ${accessToken}`
            },
        });
        const data = await response.json();
        if (response.ok) {
            // show success message and update user data
            Swal.fire({
                icon: 'success',
                title: 'User promoted to admin successfully!',
                showConfirmButton: false,
                timer: 1500
            });
            setUsers(prevUsers => {
                const updatedUsers = prevUsers.map(user => {
                    if (user._id === userId) {
                        return { ...user, isAdmin: true };
                    }
                    return user;
                });
                return updatedUsers;
            });
        } else {
            // show error message
            Swal.fire({
                icon: 'error',
                title: 'Only JC can promote a User to Admin',
                text: data.error,
            });
        }
    };

    const handleDemoteClick = async (userId) => {
        const response = await fetch(`${process.env.REACT_APP_API_URL}/booknook/users/makeuser/${userId}`, {
            method: 'PUT',
            headers: {
                Authorization: `Bearer ${accessToken}`
            },
        });
        const data = await response.json();
        if (response.ok) {
            // show success message and update user data
            Swal.fire({
                icon: 'success',
                title: 'User demoted to regular user successfully!',
                showConfirmButton: false,
                timer: 1500
            });
            setUsers(prevUsers => {
                const updatedUsers = prevUsers.map(user => {
                    if (user._id === userId) {
                        return { ...user, isAdmin: false };
                    }
                    return user;
                });
                return updatedUsers;
            });
        } else {
            // show error message
            Swal.fire({
                icon: 'error',
                title: 'Only JC can demote an Admin to User',
                text: data.error,
            });
        }
    };

    const currentUserEmail = localStorage.getItem('email');

    const adminUsers = Array.isArray(users) ? users.filter((user) => user.isAdmin) : [];
    const regularUsers = Array.isArray(users)
        ? users.filter((user) => !user.isAdmin && (user.name.includes(searchQuery) || user.email.includes(searchQuery)))
        : [];

    function getAvatarTitle(user) {
        const colors = ["#FF4500", "#1E90FF", "#32CD32", "#FFA500", "purple", "#6D757A"];

        const firstLetter = user.name.slice(0, 1).toUpperCase();
        let colorIndex;
        switch (firstLetter) {
            case "A":
            case "B":
            case "C":
            case "D":
                colorIndex = 0; // use red
                break;
            case "E":
            case "F":
            case "G":
            case "H":
                colorIndex = 1; // use blue
                break;
            case "I":
            case "J":
            case "K":
            case "L":
                colorIndex = 2; // use green
                break;
            case "M":
            case "N":
            case "O":
            case "P":
                colorIndex = 3; // use orange
                break;
            case "Q":
            case "R":
            case "S":
            case "T":
                colorIndex = 4;
                break;
            case "U":
            case "V":
            case "W":
            case "X":
            case "Y":
            case "Z":
                colorIndex = 5;
        }

        const avatar = (
            <div className="avatar" style={{ backgroundColor: colors[colorIndex] }}>
                {user.name.slice(0, 2)}
            </div>
        );
        const title = (
            <React.Fragment>
                {avatar}
                {user.name}
            </React.Fragment>
        );
        return title;
    }


    return (
        <Container style={{ paddingTop: '80px', paddingBottom: '80px' }}>
            <h2 className="center-on-small"
                style={{
                    fontFamily: 'Gentium Book Plus',
                    fontWeight: 'bold',
                    fontSize: '40px',
                }}>Admins</h2>
            <Row>
                {adminUsers.map((user) => (
                    <Col key={user._id} md={4}>
                        <Card style={{ marginBottom: '10px'}}>

                            <Card.Body>
                                <Card.Title style={{ fontFamily: "Gentium Book Plus", fontWeight: 'bold'}}>
                                    {getAvatarTitle(user)}
                                </Card.Title>

                                <Card.Subtitle className="mb-2 text-muted" style={{ fontFamily: "Gentium Book Plus", }}>{user.email}</Card.Subtitle>
                                <Card.Text style={{ fontFamily: "Gentium Book Plus",}}>
                                    Mobile No: <b>{user.mobileNo}</b>
                                    <br />
                                    Registered On: <b>{new Date(user.createdOn).toLocaleString()}</b>
                                </Card.Text>
                            </Card.Body>
                            <div style={{ display: 'flex', justifyContent: 'Center' }}>
                                {user.email === 'mrjcdelizo@gmail.com' ? (
                                    <Button variant="dark" style={{ flex: 1, borderRadius: '0 0 5px 5px' }} >SUPER ADMIN</Button>
                                ) : (
                                    user.isAdmin ? (
                                        <Button variant="danger" onClick={() => handleDemoteClick(user._id)} style={{ flex: 1, borderRadius: '0 0 5px 5px' }} >Demote to User</Button>
                                    ) : (
                                        <Button variant="success" onClick={() => handlePromoteClick(user._id)} style={{ flex: 1, borderRadius: '0 0 5px 5px' }} >Promote to Admin</Button>
                                    )
                                )}
                            </div>
                        </Card>
                    </Col>
                ))}
            </Row>
            <hr />
            <div style={{ display: 'flex', justifyContent: 'space-between', marginBottom: '10px' }}>
                <h2 className="center-on-small"
                    style={{
                        fontFamily: 'Gentium Book Plus',
                        fontWeight: 'bold',
                        fontSize: '40px',
                    }}>Users</h2>
                <input
                    type="text"
                    placeholder="Search users by name or email"
                    value={searchQuery}
                    onChange={(e) => setSearchQuery(e.target.value)}
                    style={{
                        paddingLeft: '10px',
                        paddingRight: '10px',
                        paddingTop: '5px',
                        paddingBottom: '5px',
                        fontFamily: "Gentium Book Plus",
                        fontSize: '18px',
                        fontWeight: 'bold',
                    }}
                />
            </div>
            <Row>
                {regularUsers.map((user) => (
                    <Col key={user._id} md={4}>
                        <Card style={{ marginBottom: '10px'}}>
                            <Card.Body>
                                <Card.Title style={{ fontFamily: "Gentium Book Plus", fontWeight: 'bold'}}>
                                    {getAvatarTitle(user)}
                                </Card.Title>

                                <Card.Subtitle className="mb-2 text-muted" style={{ fontFamily: "Gentium Book Plus", }}>{user.email}</Card.Subtitle>
                                <Card.Text style={{ fontFamily: "Gentium Book Plus", }}>
                                    Mobile No: <b>{user.mobileNo}</b>
                                    <br />
                                    Registered On: <b>{new Date(user.createdOn).toLocaleString()}</b>
                                </Card.Text>
                            </Card.Body>
                            <div style={{ display: 'flex', justifyContent: 'Center' }}>
                                {currentUserEmail === 'mrjcdelizo@gmail.com' ? (
                                    <div> </div>
                                ) : (
                                    user.isAdmin ? (
                                        <Button variant="danger" onClick={() => handleDemoteClick(user._id)} style={{ flex: 1, borderRadius: '0 0 5px 5px' }} >Demote to User</Button>
                                    ) : (
                                        <Button variant="success" onClick={() => handlePromoteClick(user._id)} style={{ flex: 1, borderRadius: '0 0 5px 5px' }} >Promote to Admin</Button>
                                    )
                                )}
                            </div>
                        </Card>
                    </Col>
                ))}
            </Row>
        </Container>

    );





}

export default ViewUsers;
