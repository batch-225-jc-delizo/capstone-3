import React, { useContext, useState, useEffect } from 'react';
import { Container, Row, Col, ListGroup, Card, Button } from 'react-bootstrap';
import { useParams, useNavigate } from 'react-router-dom';
import Loading from '../components/Loading';
import UserContext from '../UserContext';
import '../App.css';

const BookPage = () => {
	const { user } = useContext(UserContext);
  const { id } = useParams();
  const [book, setBook] = useState(null);
  const navigate = useNavigate();
  const [isAddedToCart, setIsAddedToCart] = useState(false);
  const [isLoading, setIsLoading] = useState(true); // state to track loading status
  const [recommendedBooks, setRecommendedBooks] = useState([]);

  useEffect(() => {
    const fetchBookData = async () => {
      try {
        const res = await fetch(`${process.env.REACT_APP_API_URL}/booknook/books/specificbook/${id}`);
        const data = await res.json();
        setBook(data);
      } catch (err) {
        console.log(err);
      } finally {
        setIsLoading(false); // set loading status to false when data is fetched
      }
    };

    const fetchRecommendedBooks = async () => {
      try {
        const res = await fetch(`${process.env.REACT_APP_API_URL}/booknook/books/activebooks`);
        const data = await res.json();
        setRecommendedBooks(data.filter((b) => b.genre === book?.genre && b._id !== book?._id).slice(0, 4));
      } catch (err) {
        console.log(err);
      }
    };

    fetchBookData();
    fetchRecommendedBooks();
  }, [id, book?.genre, book?._id]);

  const handleAddToCart = async () => {
    setIsAddedToCart(true);
    try {
      const accessToken = localStorage.getItem('access');
      if (!accessToken) {
        // User is not logged in, navigate to login page
        navigate('/login');
        return;
      }

      const response = await fetch(`${process.env.REACT_APP_API_URL}/booknook/cart/add-to-cart/${book._id}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${accessToken}`
        },
        body: JSON.stringify({ bookId: book._id, quantity: 1 }),
      });
      if (!response.ok) {
        throw new Error('Failed to add item to cart');
      }
    } catch (error) {
      console.error(error);
    } finally {
      setTimeout(() => setIsAddedToCart(false), 2000);
    }
  };

  if (isLoading || !book) { // display loading component while data is being fetched
    return <Loading />;
  }

  const handleClickScroll = () => {
    window.scrollTo(0, 0);
  }
  

  return (
    <>
      {/* <div style={{ paddingTop: '70px' }}></div> */}
      <Container style={{ maxWidth: '100%', marginLeft: 0, marginRight: 0 }}>
        <Row style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'space-around', alignItems: 'flex-start' }}>
          <Col lg={7} className='p-0' style={{ marginTop: '70px' }} id='top'>
            <Card className='p-0'>
              <Row>
                <Col xs={{ span: 12, order: 1 }} md={{ span: 8, order: 1 }}>
                  <Card.Img src={book.image} alt={book.name} style={{ objectFit: "cover", height: "100%", borderRadius: '5px 0 0 0' }} />
                </Col>
                <Col xs={{ span: 12, order: 2 }} md={{ span: 4, order: 2 }} style={{ paddingBottom: 0 }} >
                  <Card.Body className='px-3 pr-md-0'>
                    <Card.Title
                      style={{
                        fontFamily: 'Gentium Book Plus',
                        fontWeight: 'bold',
                        fontSize: '40px',
                        marginTop: '10px'
                      }}
                    >{book.name}</Card.Title>
                    <Card.Text>{book.description}</Card.Text>
                    <ListGroup.Item>Author: <span style={{ fontFamily: 'Gentium Book Plus', fontWeight: 'bold', fontSize: '18px' }}>{book.author}</span></ListGroup.Item>
                    <ListGroup.Item>Genre: <span style={{ fontFamily: 'Gentium Book Plus', fontWeight: 'bold', fontSize: '18px' }}>{book.genre}</span></ListGroup.Item>
                    <Card.Text style={{ marginBottom: '16px', marginTop: '16px' }}><span style={{ fontFamily: 'Roboto', fontWeight: 'bold', fontSize: '18px', color: '#F78124' }}>₱{book.price}.00</span></Card.Text>
                  </Card.Body>
                </Col>
              </Row>
              <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                <Button 
                  variant="success" 
                  onClick={() => navigate('/books')} 
                  style={{ 
                    borderRadius: '0 0 0 5px',
                    borderRadius: user.isAdmin ? '0 0 5px 5px' : '0 0 0 5px', 
                    flex: 1, 
                    fontWeight: 'bold', 
                    fontFamily: 'Roboto', 
                  }}>Go to Books</Button>
                <Button
                  onClick={handleAddToCart}
                  className="darkhover"
                  variant="primary"
                  disabled={isAddedToCart}
                  style={{
                    borderRadius: '0 0 5px 0',
                    flex: 1,
                    backgroundColor: '#F78124',
                    borderColor: '#F78124',
                    fontWeight: 'bold',
                    fontFamily: 'Roboto',
                    display: user.isAdmin ? 'none' : 'block',
                  }}
                  activestyle={{
                    backgroundColor: '#FABE9B',
                    borderColor: '#FABE9B',
                    fontWeight: 'bold',
                    fontFamily: 'Roboto',
                    display: user.isAdmin ? 'none' : 'block',
                  }}
                >
                  {isAddedToCart ? 'Added to Cart!' : 'Add to Cart'}
                </Button>

              </div>

            </Card>



          </Col>
          <Col lg={4} className='pl-0 pr-0 pb-0 pt-3 pt-md-4 mt-md-5 mt-3'>
            {isLoading ? (
              <Loading />
            ) : (
              <Card className='mb-5'>
                <Card.Body>
                  <Card.Title className='mb-3' style={{ fontFamily: 'Gentium Book Plus', fontWeight: 'bold'}}>More books from {book.genre}!</Card.Title>
                  <div style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'space-around', alignItems: 'center' }}>
                    {recommendedBooks.map((book) => (
                      <Card
                        key={book._id}
                        className='mb-3 hoverme recom-card'
                        onClick={() => { navigate(`/bookpage/${book._id}`); handleClickScroll();}}
                        style={{
                          boxShadow: '0px 4px 8px rgba(0, 0, 0, 0.15)',
                          transition: 'transform 0.15s ease-in-out',
                          maxWidth: '200px',
                        }}
                      >
                        <Card.Img
                          src={book.image}
                          className='recom-img'
                          alt={book.name}
                          fluid="true"
                          style={{
                            height: '150px',
                            objectFit: 'cover',
                            cursor: 'pointer',
                          }}
                        />
                        <Card.Header
                          style={{
                            fontWeight: 'bold',
                            WebkitLineClamp: '1',
                            overflow: 'hidden',
                            textOverflow: 'ellipsis',
                            display: '-webkit-box',
                            WebkitBoxOrient: 'vertical',
                            fontFamily: 'Gentium Book Plus',
                            paddingLeft: '10px',
                            paddingRight: '10px',
                            paddingTop: '5px',
                            paddingBottom: '5px',
                            cursor: 'pointer',
                          }}
                        >
                          {book.name}
                        </Card.Header>
                        <Card.Body className='home-featurecard'
                          style={{
                            paddingLeft: '10px',
                            paddingRight: '10px',
                            paddingTop: '5px',
                            paddingBottom: '5px',
                            cursor: 'pointer',
                          }}
                        >
                          <Card.Text
                            style={{
                              WebkitLineClamp: '2',
                              overflow: 'hidden',
                              textOverflow: 'ellipsis',
                              display: '-webkit-box',
                              WebkitBoxOrient: 'vertical',
                              fontSize: '12px',
                              padding: 0,
                              margin: 0,
                            }}
                          >
                            {book.description}
                          </Card.Text>
                        </Card.Body>
                        <Card.Footer
                          style={{
                            display: 'flex',
                            justifyContent: 'space-between',
                            cursor: 'pointer',
                            paddingLeft: '10px',
                            paddingRight: '10px',
                            paddingTop: '5px',
                            paddingBottom: '5px',
                          }}
                        >
                          <Card.Text
                            style={{
                              fontWeight: 'bold',
                              fontFamily: 'Roboto',
                              color: '#F78124',
                            }}
                          >
                            ₱{book.price}.00
                          </Card.Text>
                        </Card.Footer>
                      </Card>
                    ))}
                  </div>
                </Card.Body>
              </Card>

            )}
          </Col>

        </Row>
      </Container>
    </>
  );


};

export default BookPage;
