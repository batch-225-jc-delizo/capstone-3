import React, { useState, useEffect } from 'react';
import { Button, Dropdown, DropdownButton, Col, Row, Card } from "react-bootstrap";
import UpdateBook from '../components/UpdateBook';
import { useNavigate } from 'react-router-dom';
import AddBook from '../components/AddBook';
import AdminInfo from '../components/AdminInfo';
import { FaTrash, FaEye, FaEyeSlash, FaQuestion, FaSortAlphaDown, FaSortAlphaUp } from 'react-icons/fa';
import '../App.css'

export default function BookArchives(props) {
  const [books, setBooks] = useState([]);
  const [sortOrder, setSortOrder] = useState('asc');
  const [nameSortOrder, setNameSortOrder] = useState('asc');
  const [selectedGenre, setSelectedGenre] = useState('');
  const [selectedAvailability, setSelectedAvailability] = useState('all');
  const [originalBooks, setOriginalBooks] = useState([]);
  const [showUpdateModal, setShowUpdateModal] = useState(false);
  const [showInfoModal, setShowInfoModal] = useState(false);
  const [selectedBook, setSelectedBook] = useState(null);
  const [showAddModal, setShowAddModal] = useState(false);
  const [searchTerm, setSearchTerm] = useState('');
  const accessToken = localStorage.getItem('access');
  const navigate = useNavigate();

  const availabilityOptions = [
    { value: 'all', label: 'All' },
    { value: 'available', label: 'Available' },
    { value: 'unavailable', label: 'Unavailable' },
  ];

  // Fetch books data from API
  const fetchBooks = async () => {
    try {
      const url = `${process.env.REACT_APP_API_URL}/booknook/books`;
      const response = await fetch(url, {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      });
      const data = await response.json();
      if (Array.isArray(data)) {
        let filteredData = data.filter((book) => book.name.toLowerCase().includes(searchTerm.toLowerCase()));
        if (selectedGenre) {
          filteredData = filteredData.filter((book) => book.genre === selectedGenre);
        }
        setBooks(filteredData);
        filteredData.reverse();
        setOriginalBooks(data);
        data.reverse();
      } else {
        console.log('Response is not an array:', data);
      }
    } catch (error) {
      console.log('Error fetching books:', error);
    }
  };


  // fetch books on component mount
  useEffect(() => {
    fetchBooks();
  }, []);

  // handlers for updating a book
  const handleUpdate = (book) => {
    setSelectedBook(book);
    setShowUpdateModal(true);
    console.log("Add New Book button clicked")
  };
  const handleCloseUpdateModal = () => {
    setShowUpdateModal(false);
    setSelectedBook(null);
  };
  const handleBookUpdated = () => {
    fetchBooks();
  };

  // Handler for deleting a book
  const handleDelete = (book) => {
    const confirmed = window.confirm(`Are you sure you want to delete "${book.name}"?`);
    if (!confirmed) {
      return;
    }

    const url = `${process.env.REACT_APP_API_URL}/booknook/books/${book._id}`;
    fetch(url, {
      method: 'DELETE',
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        // Refresh the book list
        fetchBooks();
      })
      .catch((error) => {
        console.log('Error deleting book:', error);
      });
  };

  // Handler for disabling or enabling a book
  const handleDisable = (book) => {
    const updatedBook = {
      ...book,
      isActive: !book.isActive
    };

    const url = book.isActive
      ? `${process.env.REACT_APP_API_URL}/booknook/books/archive/${book._id}`
      : `${process.env.REACT_APP_API_URL}/booknook/books/activate/${book._id}`;

    fetch(url, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${accessToken}`
      },
      body: JSON.stringify(updatedBook)
    })
      .then(response => response.json())
      .then(data => {
        // Update the books state to reflect the change
        const updatedBooks = books.map(b => {
          if (b._id === book._id) {
            return updatedBook;
          }
          return b;
        });
        setBooks(updatedBooks);
      })
      .catch(error => {
        console.log('Error updating book:', error);
      });
  };

  // Handler for searching books
  const handleSearch = (event) => {
    const searchTerm = event.target.value.toLowerCase();
    const filteredBooks = originalBooks.filter((book) => {
      return (
        book.name.toLowerCase().includes(searchTerm) ||
        book.description.toLowerCase().includes(searchTerm)
      );
    });
    setBooks(filteredBooks);
    setSearchTerm(searchTerm);
    setSelectedGenre('');
  };

  // Handle sorting by book name
  const handleSortByName = () => {
    const sortedBooks = [...books].sort((a, b) => {
      if (nameSortOrder === 'asc') {
        return a.name.localeCompare(b.name);
      } else {
        return b.name.localeCompare(a.name);
      }
    });
    setBooks(sortedBooks);
    setNameSortOrder(nameSortOrder === 'asc' ? 'desc' : 'asc');
    setSelectedGenre('');
  };

  // Handler for selecting new Genre state
  const handleFilterByGenre = (eventKey) => {
    setSelectedGenre(eventKey);
    const filteredData = originalBooks.filter((book) => {
      return (
        book.name.toLowerCase().includes(searchTerm.toLowerCase()) &&
        (eventKey === '' || book.genre === eventKey)
      );
    });
    setBooks(filteredData);
    setSelectedGenre(eventKey);
  };

  const getGenres = () => {
    const genres = originalBooks.map((book) => book.genre);
    return [...new Set(genres)];
  };

  // Handler for selecting an availability option
  const handleAvailabilityChange = (value) => {
    setSelectedAvailability(value);
    if (value === 'all') {
      setBooks(originalBooks);
    } else if (value === 'available') {
      setBooks(originalBooks.filter(book => book.isActive));
    } else if (value === 'unavailable') {
      setBooks(originalBooks.filter(book => !book.isActive));
    }
  };

  // Handler for info modal
  const handleInfoClick = () => {
    setShowInfoModal(true);
  };




  return (
    <div style={{ paddingTop: '80px', paddingBottom: '30px', display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
      <div style={{ marginBottom: '20px', width: '100%' }}>
        <Row className='mx-1 mx-md-4'>
          <Col xs={9} md={10}>
            <input
              type="text"
              placeholder="Search books"
              value={searchTerm}
              onChange={handleSearch}
              style={{
                paddingLeft: '15px',
                paddingTop: '10px',
                paddingBottom: '10px',
                fontFamily: "Gentium Book Plus",
                fontSize: '15px',
                fontWeight: 'bold',
                width: '100%',
                height: '100%',
              }}
            />
          </Col>
          <Col xs={3} md={2}>
            <Button
              variant='success'
              onClick={() => setShowAddModal(true)}
              style={{
                fontFamily: "Gentium Book Plus",
                fontWeight: 'bold',
                fontSize: '15px',
                width: '100%',
                height: '100%',
                borderRadius: '0',
              }}>
              New Book
            </Button>
          </Col>
        </Row>
        <div style={{ display: 'flex', justifyContent: 'space-between', paddingTop: '8px', paddingBottom: '8px', paddingRight: '3px', paddingLeft: '3px' }} className='mx-md-4'>
          <div onClick={handleInfoClick} style={{ marginRight: '5px', marginLeft: '10px' }}>
            <Button variant="light">
              <FaQuestion />
            </Button>
          </div>
          <div style={{ display: 'flex', justifyContent: 'flex-end', marginRight: '5px' }}>
            <div style={{ marginRight: '5px', marginLeft: '5px' }}>
              <Button onClick={handleSortByName} variant='light'>
                {nameSortOrder === 'asc' ? <FaSortAlphaDown style={{ marginBottom: '2px' }} /> : <FaSortAlphaUp style={{ marginBottom: '2px' }} />}
              </Button>
            </div>
            <div style={{ marginRight: '5px', marginLeft: '5px' }}>
              <DropdownButton
                title="Availability"
                variant="light"
              >
                {availabilityOptions.map((option) => (
                  <Dropdown.Item
                    key={option.value}
                    onClick={() => handleAvailabilityChange(option.value)}
                  >
                    {option.label}
                  </Dropdown.Item>
                ))}
              </DropdownButton>
            </div>
            <div style={{ marginRight: '5px', marginLeft: '5px' }}>
              <DropdownButton
                id="dropdown-genre"
                title={selectedGenre || 'All Genres'}
                variant='light'
              >
                <Dropdown.Item onClick={() => handleFilterByGenre('')}>
                  All Genres
                </Dropdown.Item>
                {getGenres().map((genre) => (
                  <Dropdown.Item
                    key={genre}
                    onClick={() => handleFilterByGenre(genre)}
                  >
                    {genre}
                  </Dropdown.Item>
                ))}
              </DropdownButton>
            </div>
          </div>
        </div>

      </div>

      <div style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'center' }}>
        {books.map((book) => (
          <Card key={book._id} className='hoverme'
            style={{
              position: 'relative',
              width: '200px',
              marginLeft: '2px',
              marginRight: '2px',
              marginTop: '10px',
              borderRadius: '0',
              boxShadow: '0px 4px 8px rgba(0, 0, 0, 0.15)',
              transition: 'transform 0.15s ease-in-out',
            }}>
            <Button
              variant='danger'
              onClick={() => handleDelete(book)}
              style={{
                position: 'absolute',
                top: '5px',
                right: '5px',
                width: '30px',
                height: '30px',
                borderRadius: '50%',
                padding: 0,
                margin: 0,
                zIndex: 1,
              }}>
              <FaTrash style={{ marginBottom: '2px' }} />
            </Button>
            <Card.Img src={book.image} alt={book.name} onClick={() => handleUpdate(book)} fluid
              style={{
                height: '150px',
                objectFit: 'cover',
                borderRadius: '0',
                cursor: 'pointer',
              }} />
            <Card.Header onClick={() => handleUpdate(book)} style={{
              maxWidth: '200px',
              backgroundColor: book.isActive ? '#fff' : 'lightgray',
              fontWeight: 'bold',
              WebkitLineClamp: '1',
              overflow: 'hidden',
              textOverflow: 'ellipsis',
              display: '-webkit-box',
              WebkitBoxOrient: 'vertical',
              fontFamily: "Gentium Book Plus",
              paddingLeft: '10px',
              paddingRight: '10px',
              paddingTop: '5px',
              paddingBottom: '5px',
              cursor: 'pointer',
            }}>{book.name}
            </Card.Header>
            <Card.Footer
              style={{
                maxWidth: '200px',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: book.isActive ? '#fff' : 'lightgray',
                padding: '10px',
                borderRadius: '0',
                gap: '4px',
                cursor: 'pointer',
              }}
              onClick={() => handleDisable(book)}
              variant={book.isActive ? 'warning' : 'success'}>
              {book.isActive ? <FaEye /> : <FaEyeSlash />}
            </Card.Footer>
          </Card>

        ))}
      </div>
      {showUpdateModal && (
        <UpdateBook
          show={showUpdateModal}
          onClose={handleCloseUpdateModal}
          book={selectedBook}
          accessToken={props.accessToken}
          onBookUpdated={handleBookUpdated}
        />
      )}
      {showAddModal && (
        <AddBook
          show={showAddModal}
          onClose={() => setShowAddModal(false)}
          accessToken={accessToken}
          onBookAdded={fetchBooks}
        />
      )}
      {showInfoModal && (
        <AdminInfo
          show={showInfoModal}
          onClose={() => setShowInfoModal(false)}
        />
      )}

    </div>
  );
}
