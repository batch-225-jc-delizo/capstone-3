import { Form, Button, Card, Col, Row } from 'react-bootstrap'
import { useState, useEffect, useContext } from 'react'
import { useNavigate, Navigate } from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'
import { Link } from 'react-router-dom';
import loginpage from '../assets/loginpage.png';
import readwell from '../assets/readwell.svg';

export default function Register() {
	// Activity
	const { user, setUser } = useContext(UserContext)
	const navigate = useNavigate()
	// Activity END


	const [name, setName] = useState('')
	const [mobileNumber, setMobileNumber] = useState('')

	const [email, setEmail] = useState('')
	const [password1, setPassword1] = useState('')
	const [password2, setPassword2] = useState('')

	// For determining if button is disabled or not
	const [isActive, setIsActive] = useState(false)


	function registerUser(event) {
		event.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/booknook/users/check-email`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
			.then(response => response.json())
			.then(result => {
				if (result.success === true) {
					Swal.fire({
						title: 'Oops!',
						icon: 'error',
						text: 'Email already exist!'
					})
				} else {

					fetch(`${process.env.REACT_APP_API_URL}/booknook/users/register`, {
						method: 'POST',
						headers: {
							'Content-Type': 'application/json'
						},
						body: JSON.stringify({
							name: name,
							mobileNo: mobileNumber,
							email: email,
							password: password1
						})
					})
						.then(response => response.json())
						.then(result => {
							console.log(result);

							setEmail('');
							setPassword1('');
							setPassword2('');
							setName('');
							setMobileNumber('');

							if (result) {
								Swal.fire({
									title: 'Registered!',
									icon: 'success',
									text: 'Are you a book maniac?'
								})

								navigate('/login')

							} else {
								Swal.fire({
									title: 'Registration Failed',
									icon: 'error',
									text: "Read Book at a Nook"
								})
							}
						})
				}
			})

	}

	useEffect(() => {
		if ((name !== '' && mobileNumber && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)) {
			// Enables the submit button if the form data has been verified
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [name, mobileNumber, email, password1, password2])

	return (
		(user.id !== null) ?
			<Navigate to="/courses" />
			:
			<>
				<div style={{ margin: 0, padding: 0 }}>
					<Row>
						<Col md={8} style={{ padding: 0 }}>
							<img src={loginpage} className='login-sm-gone' alt="Login Page" style={{ height: '100vh', width: 'auto' }} />
						</Col>
						<Col md={4} className="login-page" style={{ padding: 0, backgroundColor: '#FFFEF5', paddingRight: '30px', paddingLeft: '30px' }}>
							<div style={{ marginTop: '80px', display: 'block', justifyContent: 'center' }}>
								<img src={readwell} alt="readwell" className='login-sm-gone' style={{ height: '30px' }} />
							</div>
							<p className='login-sm-gone' style={{ maxWidth: '400px', marginTop: '20px', fontFamily: 'Roboto Slab', fontSize: '18px' }}>
								"If you don't like to read, <br />you haven't found the right book." <br />- J.K. Rowling
							</p>
                            <div className='login-padding'></div>
							<div>
								<Card className='login-center' style={{
									maxWidth: '400px',
									marginTop: '20px',
									backgroundColor: "#e2eafc",
									boxShadow: '0px 2px 4px rgba(0, 0, 0, 1)',
									borderRadius: '0'
								}}>
									<Card.Body>
										<Form onSubmit={event => registerUser(event)}>

											<Form.Group controlId="lastName" className='px-4 mt-3'>
												<Form.Control
													type="text"
													placeholder="Enter Name"
													value={name}
													onChange={event => setName(event.target.value)}
													required
													style={{
														backgroundColor: '#edf2fb',
														borderColor: '#edf2fb',
														outline: 'none',
														boxShadow: 'none',
														fontFamily: 'Roboto Slab'
													}}
												/>
											</Form.Group>

											<Form.Group controlId="mobileNumber" className='px-4 mt-1'>
												<Form.Control
													type="text"
													placeholder="Enter Mobile Number"
													value={mobileNumber}
													onChange={event => setMobileNumber(event.target.value)}
													required
													style={{
														backgroundColor: '#edf2fb',
														borderColor: '#edf2fb',
														outline: 'none',
														boxShadow: 'none',
														fontFamily: 'Roboto Slab'
													}}
												/>
											</Form.Group>

											<Form.Group controlId="userEmail" className='px-4 mt-1'>
												<Form.Control
													type="email"
													placeholder="Enter Email Address"
													value={email}
													onChange={event => setEmail(event.target.value)}
													required
													style={{
														backgroundColor: '#edf2fb',
														borderColor: '#edf2fb',
														outline: 'none',
														boxShadow: 'none',
														fontFamily: 'Roboto Slab'
													}}
												/>
											</Form.Group>

											<Form.Group controlId="password1" className='px-4 mt-3'>
												<Form.Control
													type="password"
													placeholder="Enter Password"
													value={password1}
													onChange={event => setPassword1(event.target.value)}
													required
													style={{
														backgroundColor: '#edf2fb',
														borderColor: '#edf2fb',
														outline: 'none',
														boxShadow: 'none',
														fontFamily: 'Roboto Slab'
													}}
												/>
											</Form.Group>

											<Form.Group controlId="password2" className='px-4 mt-1'>
												<Form.Control
													type="password"
													placeholder="Verify Password"
													value={password2}
													onChange={event => setPassword2(event.target.value)}
													required
													style={{
														backgroundColor: '#edf2fb',
														borderColor: '#edf2fb',
														outline: 'none',
														boxShadow: 'none',
														fontFamily: 'Roboto Slab'
													}}
												/>
											</Form.Group>
											<div style={{ display: 'flex', justifyContent: 'center', marginTop: '10px' }} className='px-4'>
												{isActive ?
													<Button variant="success" type="submit" id="submitBtn"
														style={{
															paddingLeft: '30px',
															paddingRight: '30px',
															fontFamily: 'Roboto Slab',
                                                            width: '100%'
														}} >
														ENTER THE WORLD OF BOOKS
													</Button>
													:
													<Button variant="secondary" type="SUBMIT" id="submitBtn" disabled
														style={{
															paddingLeft: '30px',
															paddingRight: '30px',
															fontFamily: 'Roboto Slab',
                                                            width: '100%'
														}}>
														REGISTER
													</Button>
												}
											</div>
											<div className="text-center mt-3 mb-3">
												<Form.Text style={{ color: 'black' }}>
													Already have an account? <Link to="/login" style={{ textDecoration: 'none', color: '#0A58CA' }}>Log in</Link>.
												</Form.Text>
											</div>


										</Form>
									</Card.Body>
								</Card>
							</div>
							<p className='login-sm-gone' style={{ maxWidth: '400px', marginTop: '20px', fontFamily: 'Roboto Slab', fontSize: '18px' }}>
								"Reading is to the mind <br/>what exercise is to the body." <br/>- Joseph Addison
							</p>
						</Col>
					</Row>
				</div>
			</>
	)
}
