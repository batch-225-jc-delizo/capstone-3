import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css'; 
import { useState, useEffect } from 'react';
import { UserProvider } from './UserContext';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Books from './pages/Books';
import BookPage from './pages/BookPage';
import Register from './pages/Register';
import MustLogIn from './pages/MustLogIn';
import Login from './pages/Login';
import Logout from './pages/Logout';
import BookArchives from './pages/BookArchives';
import ViewOrders from './pages/ViewOrders';
import ViewUsers from './pages/Users';
import Cart from './pages/Cart';
import Orders from './pages/Orders';
import ErrorPage from './pages/ErrorPage';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/booknook/users/details`, {
      headers: {
        Authorization: `Bearer ${ localStorage.getItem('token') }`
      }
    })
    .then(res => res.json())
    .then(data => {
      if (typeof data._id !== "undefined") {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        });
      } else {
        setUser({
          id: null,
          isAdmin: null
        });
      }
    });
  }, []);

  return (
    <>
    <div style={{ backgroundColor: '#FFFEF5' }}>
      <UserProvider value={{user, setUser, unsetUser}}>
        <Router>
          <AppNavbar />
          <Container style={{ minHeight: '100vh', marginRight: 0, marginLeft: 0, maxWidth: '100%' }}>
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/books" element={<Books />} />
              <Route path="/mustlogin" element={<MustLogIn />} />
              <Route path="/bookpage/:id" element={<BookPage />} />
              <Route path="/book-archives" element={<BookArchives />} />
              <Route path="/view-orders" element={<ViewOrders />} />
              <Route path="/view-users" element={<ViewUsers />} />
              <Route path="/cart" element={<Cart />} />
              <Route path="/orders" element={<Orders />} />
              <Route path="/login" element={<Login />} />
              <Route path="/register" element={<Register />} />
              <Route path="/logout" element={<Logout />} />
              <Route path="*" element={<ErrorPage />} />
            </Routes>
          </Container>
        </Router>
      </UserProvider>
      </div>
    </>
    
  );
}

export default App;
